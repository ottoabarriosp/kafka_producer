import { Controller, Get, Logger, Req, UseInterceptors } from "@nestjs/common";
import { Client, ClientKafka, Transport } from "@nestjs/microservices";
import { Request } from "express";
import { AppService } from "./app.service";
import { SchemaRegistry } from "@kafkajs/confluent-schema-registry";
import { MessageSendDto } from "./dto";

@Controller()
export class AppController {
  constructor(
    private readonly appService: AppService,
  ) {}
  private readonly logger = new Logger(AppController.name);
  private readonly registry = new SchemaRegistry({ host: "http://localhost:8081" });

  @Client({
    transport: Transport.KAFKA,
    options: {
      client: {
        clientId: "kafka-producer",
        brokers: ["localhost:9092"],
      },
      consumer: {
        groupId: "test",
      },
    },
  })
  client: ClientKafka;

  async onModuleInit() {
    const TOPICS = ["test", "actions", "products", "errors", "jobdata"];
    TOPICS.map((topic) => this.client.subscribeToResponseOf(topic));
    await this.client.connect();
  }

  @Get("test")
  async sendTest(): Promise<any> {
    this.logger.debug("[Test Producer]");
    try {
      return this.client.send(
        "test",
        "mensaje de prueba " + new Date().toString()
      );
    } catch (err) {
      this.logger.error(err.message || err, err.stack);
    }
  }

  @Get("actions")
  async sendActions(): Promise<any> {
    const data = new MessageSendDto({
      signal: 'start',
      target: 'MS_ALKEMICS'
    });
    this.logger.debug("[Actions Producer]");
    try {
      const encode = await this.registry.encode(1, data);
      return this.client.send("actions", encode);
    } catch (err) {
      this.logger.error(err.message || err, err.stack);
    }
  }

  @Get("products")
  async sendProduct(@Req() req: Request): Promise<any> {
    this.logger.debug("[Products Producer]");
    const { id } = req.query;
    const schemaId = id && parseInt(id as string);
    const data = new MessageSendDto(req.body);
    try {
      const encode = await this.registry.encode(schemaId, data);
      return this.client.send("products", encode);
    } catch (err) {
      this.logger.error(err.message || err, err.stack);
    }
  }

  @Get("errors")
  async sendErrors(@Req() req: Request): Promise<any> {
    this.logger.debug("[Errors Producer]");
    const { id } = req.query;
    const schemaId = id && parseInt(id as string);
    const data = new MessageSendDto(req.body);
    try {
      const encode = await this.registry.encode(schemaId, data);
      return this.client.send("errors", encode);
    } catch (err) {
      this.logger.error(err.message || err, err.stack);
    }
  }

  @Get("jobdata")
  async sendJobdata(@Req() req: Request): Promise<any> {
    this.logger.debug("[Jobdata Producer]");
    const { id } = req.query;
    const schemaId = id && parseInt(id as string);
    const data = new MessageSendDto(req.body);
    try {
      const encode = await this.registry.encode(schemaId, data);
      return this.client.send("jobdata", encode);
    } catch (err) {
      this.logger.error(err.message || err, err.stack);
    }
  }
}
